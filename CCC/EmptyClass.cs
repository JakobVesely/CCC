﻿using System;
using System.Collections;
using System.Linq;

namespace CCC
{


    public class Normalize
    {
        public static ImageCollection normalize(ImageCollection ic)
        {
            ImageCollection nic = new ImageCollection(ic.start,ic.end);
            foreach (Image i in ic.images)
            {
                Image ni = normalizeImage(i);
                nic.addImage(ni);
            }

            return ic;
        }

        public static Image normalizeImage(Image img)
        {
            ArrayList rowIndizes = new ArrayList();
            ArrayList colIndizes = new ArrayList();

            for (int i = 0; i < img.rowcount; i++)
            {
                for (int j = 0; j < img.colcount; j++)
                {
                    if(img.matrix[i,j] != 0){
                        if(!rowIndizes.Contains(i))
                            rowIndizes.Add(i);
                    }
                }
            }

            int[,] tmp_arr = new int[rowIndizes.Count, img.colcount];
            int hrow = 0;
            for (int i = 0; i < img.rowcount; i++)
            {
                if (rowIndizes.Contains(i))
                {
                    for (int j = 0; j < img.colcount; j++)
                    {
                        tmp_arr[hrow, j] = img.matrix[i, j];
                    }
                    hrow++;
                }
            }

            
            for (int i = 0; i < img.colcount; i++)
            {
                for (int j = 0; j < rowIndizes.Count; j++)
                {
                    if (tmp_arr[j, i] != 0)
                    {
                        if(!colIndizes.Contains(i))
                        colIndizes.Add(i);
                    }
                }
            }

            int[,] normalized_matrix = new int[rowIndizes.Count, colIndizes.Count];
            int hcol = 0;
            for (int i = 0; i < img.colcount; i++)
            {
                if (colIndizes.Contains(i))
                {
                    for (int j = 0; j < rowIndizes.Count; j++)
                    {
                        normalized_matrix[j, hcol] = tmp_arr[j, i];
                    }
                    hcol++;
                }
            }

            Image nimg = new Image(img.timestamp, rowIndizes.Count, colIndizes.Count, normalized_matrix);

            return nimg;
        }

    }


    public class ImageTimestampComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((Image) x).timestamp.CompareTo(((Image)y).timestamp);
        }
    }

    public class Compareresult
    {
        public int first;
        public int last;
        public int count;
    }


    public class ImageCollection
    {

        public int start;
        public int end;
        public int imagecount;

        public ArrayList images;


        public void summary()
        {
            ArrayList res = new ArrayList();
       
            images.Sort(new ImageTimestampComparer());

            for (int i = 0; i < images.Count; i++)
            {
                for (int j = i; j < images.Count; j++)
                {

                }
            }
        }

        public void addImage(Image im)
        {
            images = new ArrayList();
            images.Add(im);
            imagecount++;
        }

        public ImageCollection(int start, int end)
        {
            this.start = start;
            this.end = end;
            this.imagecount = 0;
        }   

            public ImageCollection(string[] input) {

            images = new ArrayList();
            string[] meta = input[0].Split(' ');
            start = int.Parse(meta[0]);
            end = int.Parse(meta[1]);
            imagecount = int.Parse(meta[2]);

            int nextImageCollectionItem = 1;

            for (int i = 0; i < imagecount;i++){
                string[] imagemeta = input[nextImageCollectionItem].Split(' ');
                images.Add(new Image(int.Parse(imagemeta[0]), int.Parse(imagemeta[1]), int.Parse(imagemeta[2]), input.Skip(nextImageCollectionItem+1).Take(int.Parse(imagemeta[1])).ToArray()));
                nextImageCollectionItem = nextImageCollectionItem + 1 + int.Parse(imagemeta[1]);
            }
        }

        public ArrayList getNotEmptyImages(){

            ArrayList timestamps = new ArrayList();

            foreach(Image img in images){

                Boolean isNotEmpty = false;

                for (int i=0; i < img.rowcount;i++){
                    for (int j = 0; j < img.colcount; j++){
                        isNotEmpty = Convert.ToBoolean(img.matrix[i, j]) || isNotEmpty;
                    }
                }

                if (isNotEmpty) timestamps.Add(img.timestamp.ToString());

            }

            return timestamps;
        }
    }


    public class Image {
        public int timestamp;
        public int rowcount;
        public int colcount;

        public int[,] matrix;

        public Image(int timestamp, int rowcount, int colcount, string[] lines) {

            this.timestamp = timestamp;
            this.rowcount = rowcount;
            this.colcount = colcount;

            this.matrix = new int[this.rowcount, this.colcount];

            for (int i = 0; i < lines.Length;i++){

                string[] stringLine = lines[i].Split(' ');

                for (int j = 0; j < stringLine.Length;j++){
                    matrix[i, j] = int.Parse(stringLine[j]);
                }
            }


        }
        public Image(int timestamp, int rowcount, int colcount, int [,] m)
        {

            this.timestamp = timestamp;
            this.rowcount = rowcount;
            this.colcount = colcount;

            this.matrix = new int[this.rowcount, this.colcount];

            for (int i = 0; i < rowcount; i++)
            {

                for (int j = 0; j < colcount; j++)
                {
                    matrix[i, j] = m[i,j];
                }
            }
        }

        public static Boolean comapre(Image img1, Image img2){

            Boolean result = true;

            // check size
            if (img1.rowcount == img2.rowcount && img1.colcount == img2.colcount){

                // check content
                int[,] multiplied = new int[img1.rowcount, img1.colcount];

                for (int i = 0; i < img1.rowcount;i++){
                    
                    for (int j = 0; j < img1.colcount; j++){

                        if ((img1.matrix[i, j] > 0 && img2.matrix[i, j] == 0) || (img1.matrix[i, j] == 0 && img2.matrix[i, j] > 0)){
                            result = false;
                        }

                    }
                }


            }else{
                result = false;
            }

            return result;
        }
    }
}
