﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace CCC
{
    public class Challenge
    {
        public string Level1(string[] args)
        {
            string[] file = Input.readfile(args[0]);
            ImageCollection ic = new ImageCollection(file);

            ArrayList timestamps = ic.getNotEmptyImages();
            string[] ts = (string[])timestamps.ToArray(typeof(string));

            File.WriteAllLines(args[0] + ".sol", ts);

            return ts.Length.ToString();

        }
        public string Level2(string[] args)
        {
            string[] file = Input.readfile(args[0]);
            ImageCollection ic = new ImageCollection(file);

            ImageCollection nic = Normalize.normalize(ic);

            return null;
        }
        public string Level3(string[] args)
        {
            return null;
        }
        public string Level4(string[] args)
        {
            return null;
        }
        public string Level5(string[] args)
        {
            return null;
        }
    }
    class Program
    {
        static string[] level1input = {"2 1", "4 5"}; //1 set of arguments is 1 array element i.e {"1 2", "3 1", "5 4"}
        static void Solution(Func<string[], string> levelf, string[] levelinput)
        {
            for (int i = 0; i < levelinput.Length; i++)
            {
                string[] input = levelinput[i].Split(' ');
                Console.WriteLine("In: " + input);
                Console.WriteLine("Out: " + levelf(input));
            }
        }
        static void Main(string[] args)
        {
            Challenge ch = new Challenge();
            if(args.Length == 0)
            {
                Solution(ch.Level2,level1input);
            }
            else
            {
                ch.Level2(args);
            }
        }
    }
}
