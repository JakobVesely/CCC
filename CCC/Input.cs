﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCC
{
    class Input
    {
        public static string[] readfile(string file)
        {
            string[] lines = File.ReadAllLines(file);
            return lines;
        }
    }
}
