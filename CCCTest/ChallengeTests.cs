﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCC.Tests
{
    [TestClass()]
    public class ChallengeTests
    {
        [TestMethod()]
        [TestCategory("Level1")]
        public void Level1Test()
        {
            string[] input = {"lvl1-0.inp"}; //Format like in args
            string output = "3505,4352";
            Challenge ch = new Challenge();
            Assert.AreEqual(output, ch.Level1(input));
        }
        [TestMethod()]
        [TestCategory("Level2")]
        public void Level2Test()
        {
            string[] input = { };
            string output = "";
            Challenge ch = new Challenge();
            Assert.AreEqual(output, ch.Level2(input));
        }
        [TestMethod()]
        [TestCategory("Level3")]
        public void Level3Test()
        {
            string[] input = { };
            string output = "";
            Challenge ch = new Challenge();
            Assert.AreEqual(output, ch.Level3(input));
        }
        [TestMethod()]
        [TestCategory("Level4")]
        public void Level4Test()
        {
            string[] input = { };
            string output = "";
            Challenge ch = new Challenge();
            Assert.AreEqual(output, ch.Level4(input));
        }
        [TestMethod()]
        [TestCategory("Level5")]
        public void Level5Test()
        {
            string[] input = { };
            string output = "";
            Challenge ch = new Challenge();
            Assert.AreEqual(output, ch.Level5(input));
        }
    }
}